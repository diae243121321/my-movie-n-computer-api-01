from fastapi import APIRouter,Depends
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from typing import List
from config.database import Session
from models.computadora import Compu as CompuModel
from middlewares.jwt_bearer import JWTBearer
from schemas.computadora import Compu

compu_router = APIRouter()

# Metodo para obtener la lista de computadoras
@compu_router.get('/Computadora/', tags=['Computadora'], status_code=202, response_model=List[Compu] )  
def getCompu()->List[Compu]:
     db=Session()
     result=db.query(CompuModel).all()
     return JSONResponse(status_code=200, content=jsonable_encoder(result))

# Metodo para obtener la lista de computadoras por su ID
@compu_router.get('/Computadora/{id}', tags=['Computadora'] ) 
def getCompu (id: int)->dict:
     db=Session()
     result=db.query(CompuModel).filter(CompuModel.id==id).first()
     if not result:
          return JSONResponse(status_code=404, content={"message":"No se encontro"})
     return JSONResponse(status_code=202,content=jsonable_encoder(result))

# Metodo para obtener la lista de computadoras por su marca
@compu_router.get('/Computadora', tags=['Computadora'] ) 
def getCompuMarca(Marca: str)->dict:
     db=Session()
     result=db.query(CompuModel).filter(CompuModel.Marca==Marca).first()
     if not result:
          return JSONResponse(status_code=404, content={'message': 'No se encontro'})
     return JSONResponse(status_code=202, content=jsonable_encoder(result))

# Metodo para editar y actualizar los datos escritos anteriormente 
@compu_router.put('/Computadora/', tags=['Computadora'] ) 
def updateComputadora(id: int, Compu: Compu)->dict:
     db=Session();
     result=db.query(CompuModel).filter(CompuModel.id==id).first();
     if not result:
          return JSONResponse(status_code=404, content={'message':'No se encontro'})
     result.Marca=Compu.Marca
     result.Modelo=Compu.Modelo
     result.Color=Compu.Color
     result.Ram=Compu.Ram
     result.Almacenamiento=Compu.Almacenamiento
     db.commit();
     return JSONResponse(status_code=202, content={'message':'Se ha modificado con exito'})

# Metodo para Agregar Datos a la lista
@compu_router.post('/Computadora/', tags=['Computadora'] ) 
def createCompu(Compu: Compu)->dict:
     db=Session()
     new_compu=CompuModel(**Compu.model_dump())
     db.add(new_compu)
     db.commit()
     return JSONResponse(content={'message':'Se ha agregado el dispositivo de manera exitosa'})

# Metodo para eliminar datos de la lista
@compu_router.delete('/Computadora', tags=['Computadora'] ) 
def removeCompu(id: int )->dict:
     db=Session()
     result=db.query(CompuModel).filter(CompuModel.id==id).first()
     if not result:
          return JSONResponse(status_code=404, content={'message': 'No se encontro'})
     db.delete(result)
     db.commit()
     return JSONResponse(status_code=202, content={'message': 'Se ha eliminado de manera exitosa'})
