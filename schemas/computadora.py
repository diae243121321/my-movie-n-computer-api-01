from pydantic import BaseModel, Field
from typing import Optional

#Modelo de datos para la computadora
class Compu(BaseModel):
    id: Optional [int]= None
    Marca: str =Field(default="Marca del dispositivo", min_length=2, max_length=30)
    Modelo: str =Field(default="Modelo del dispositivo")
    Color: str =Field(default="Color del dispositivo")
    Ram: str =Field(default="Ex: 16 GB")
    Almacenamiento: str =Field(default="Ex: 256 GB")

    class Config:
        json_schema_extra ={
            "Example": {
                    "Marca": "Asus",
                    "Modelo": "Vivobook pro 15 Oled",
                    "Color":"Azul",
                    "Ram": "16 GB",
                    "Almacenamiento":"256 GB"
                }
            }