from fastapi import APIRouter,Path, Query, Depends
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from typing import List
from config.database import Session
from models.movie import Movie as MovieModel
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()

@movie_router.get('/movies', tags=['movies'],  status_code=200, response_model=List[Movie])
def get_Movies()-> List[Movie]:
     db=Session()
     result=MovieService(db).get_movies()
     return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/{id}', tags=['movies'],  status_code=200, response_model=List[Movie])
def get_Movies_id (id: int = Path(ge=1, le=2000))-> Movie:
     db=Session()
     result=MovieService(db).get_movie_id(id)
     if not result:
          return JSONResponse(status_code=404, content={'message': "No se encontro"})
     return JSONResponse(status_code=200, content=jsonable_encoder(result))

@movie_router.get('/movies/', tags=['movies'],  response_model=List[Movie])
def get_Movies_Category(Category: str = Query(min_length=5, max_length=25)):
     db=Session()
     result =  MovieService(db).get_movie_category(Category)
     return JSONResponse(content=jsonable_encoder(result))

@movie_router.post('/movies', tags=['movies'],  response_model=dict)
def create_movie(movie: Movie) -> dict:
     db=Session()
     #Utilizamos el modelo y le pasamos la informacion que vamos a registrar
     MovieService(db).create_movie(movie)
     return JSONResponse(content={"message": "Se ha agregado la pelicula exitosamente"})

@movie_router.put('/movies/', tags=['movies'])
def UpdateMovie(id: int, movie: Movie) -> dict:
     db=Session()
     result=MovieService(db).get_movie_id(id)
     if not result:
          return JSONResponse(status_code=404, content={'message': "No se encontro"})
     MovieService(db).update_movie(id, movie)
     return JSONResponse(status_code=202, content={"message": "Se ha modificado la pelicula"})
     
@movie_router.delete('/movies', tags=['movies'])
def removeMovies(id: int) ->dict:
     db=Session()
     result = MovieService(db).get_movie_id(id)
     if not result:
          return JSONResponse(status_code=404, content={'message': "No se encontro"})
     MovieService(db).delete_movie(id)
     return JSONResponse(status_code=202, content={'message': "Se ha eliminado con exito"})
     